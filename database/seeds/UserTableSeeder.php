<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' =>1,
                'name' => 'Admin',
                'email'=>'admin@gmail.com',
                'password'=>'$2y$10$ipvCbdYfqRF2fRXfKAShGOpaa24FGrzlCQjWq9joKLFtiQpTpDek2',
                'role'=>'admin'
            ]
         ] );
    }
}
