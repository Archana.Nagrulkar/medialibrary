<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            [
                'id' =>1,
                'name' => 'Video',
                'status' => 1
            ],
            [ 
                'id' =>2,
                'name' => 'Images',
                'status' => 1
            ],
            [
                'id' =>3,
                'name' => 'Documents',
                'status' => 1
            ]
         ] );
    }
}
