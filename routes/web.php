<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('main');
// Route::get('/media', function () {
//     return view('media/list');
// });
// Route::get('/admin', function () {
//     return view('welcome');
// });
Auth::routes();

//Route::get('/admin', 'HomeController@index')->name('home');

// oute::get('/', 'MediaController@index')->name('main');
Route::get('/media', 'MediaController@media')->name('media');
Route::get('/add-media', 'MediaController@add')->name('add-media');
Route::get('/edit-media/{id?}', 'MediaController@edit')->name('edit-media');
Route::post('/save-media', 'MediaController@save')->name('save-media');
Route::post('/update-media', 'MediaController@update')->name('update-media');
Route::get('/delete-media/{id?}', 'MediaController@delete')->name('delete-media');


// Admin route

Route::get('/admin/media', 'MediaController@adminmedia')->name('admin.media');
Route::get('/admin/add-media', 'MediaController@add')->name('admin.add-media');
Route::get('/admin/edit-media/{id?}', 'MediaController@edit')->name('admin.edit-media');
