<?php

namespace App\Http\Controllers;

use App\Category;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CommonController as Helper;
use Validator;

class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend/index');
    }

    public function media()
    {
        $list = Media::where('created_by', Auth::user()->id)->get();
        $category = Category::get();
        return view('frontend/media', compact('list', 'category'));
    }

    public function adminmedia()
    {
        $list = Media::get();
        $category = Category::get();
        return view('frontend/media', compact('list', 'category'));
    }

    public function add()
    {
        $category = Category::get();
        return view('admin/media/add', compact('category'));
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'category' => 'required',
            'media_file' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('/add-media')->withErrors($validator)->withInput();
        } else {
             $time=time();
            $category = Category::find($request->category)->name;
            if ($request->hasFile('media_file')) {
                $file = $request->file('media_file');
                $filename = Auth::user()->id.'_'.$time.'_'.$file->getClientOriginalName();
                Helper::SaveMedia($file,$filename,$category);
                $media = new Media();
                $media->title = $request->title;
                $media->description = $request->description;
                $media->category = $request->category;
                $media->media_file = $filename;
                $media->created_by = Auth::user()->id;
                $media->updated_by = Auth::user()->id;
                $media->save();
            }

            return redirect('/media');
        }
    }

    public function edit($id)
    {
        $media = Media::where('id', $id)->first();
        $category = Category::get();
        return view('admin/media/edit', compact('category', 'media'));
    }

    public function update(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'category' => 'required',
            'description' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('edit-media/' . $request->id)->withErrors($validator)->withInput();
        } else {
            $time=time();
            // Helper::SaveMedia($request->all());
            $media = Media::find($request->id);
            $filename = $media->media_file;
            $category = Category::find($request->category)->name;
            if ($request->hasFile('media_file')) {
                $file = $request->file('media_file');
                $filename = Auth::user()->id.'_'.$time.'_'.$file->getClientOriginalName();
                // $path = public_path() . '/uploads/' . $category . '/';
                if ($media->media_file != $request->media_file) {
                    unlink(public_path() . '/uploads/' . $category . '/' . $media->media_file);
                }
                Helper::SaveMedia($file,$filename,$category);
            }
            $media->title = $request->title;
            $media->description = $request->description;
            $media->category = $request->category;
            $media->media_file = $filename;
            $media->created_by = Auth::user()->id;
            $media->updated_by = Auth::user()->id;
            $media->update();

            return redirect('/media');
        }
    }
    public function delete($id)
    {
        $media = Media::findOrFail($id);
        unlink(public_path() . '/uploads/' . $media->categoryname->name . '/' . $media->media_file);
        $media->delete();
        return redirect('/media');
    }
}
