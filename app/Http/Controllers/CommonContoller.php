<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Media;

class CommonController extends Controller
{

    public static function SaveMedia($file,$filename,$category)
    {
        $path = public_path() . '/uploads/' . $category . '/';
        return $file->move($path, $filename);
    }

}
