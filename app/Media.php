<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public function categoryname() {
        return $this->belongsTo('App\Category', 'category');
    }

    public function CreatedBy() {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function UpdatedBy() {
        return $this->belongsTo('App\User', 'created_by');
    }
}
