@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Media</div>

                <div class="card-body">
                                        <button class="btn btn-sm btn-primary"><a href="{{URL::route('add-media')}}"> Add Media</a></button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection