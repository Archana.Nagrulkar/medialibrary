@extends('layouts.app')

@section('content')
<div class="main-container clearfix nav-horizontal">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="panel panel-default panel-hovered panel-stacked">
        <div class="panel panel-body" style="color:white !important;">
          <div class="row">
            <div class="border-bottom">
              <h4 class="fntBold paddLeft10">Add Media</h4>
            </div>
          </div>
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="row">
            <form role="form" class="form-horizontal" id="media_form" method="post" enctype="multipart/form-data" action="{{route('save-media')}}">
              @csrf
              <div class="form-group">
                <label for="exampleInputEmail1">Media Title</label>
                <input type="text" class="form-control" id="mediatitle" name="title" aria-describedby="emailHelp" placeholder="Enter Media" style="color:white;">

              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Category</label>
                <Select class="form-control" name="category" style="color:white;">
                  <option value=0>Select Category</option>

                  @if($category)
                  @foreach($category as $k=>$v)
                  <option value="{{$v->id}}">{{$v->name}}</option>
                  @endforeach
                  @endif

                </Select>

              </div>
              <div class="form-group ">
                <label class="form-label" for="exampleCheck1">Browse Media</label>
                <input type="file" class="form-control" id="media" name="media_file" style="color:white;">
              </div>
              <div class="form-group">
                <label class="form-label" for="exampleCheck1">Description</label>
                <textarea class="form-control" id="media" name="description" style="color:white;"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection