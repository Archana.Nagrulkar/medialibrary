@extends('layouts.app')

@section('content')
<div class="main-container clearfix nav-horizontal">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default panel-hovered panel-stacked">
                <div class="panel panel-body">
                    <div class="row">
                        <div class="border-bottom">
                            <h4 class="fntBold paddLeft10">Edit Media</h4>
                        </div>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="row">
                        <form role="form" class="form-horizontal" id="media_form" method="post" enctype="multipart/form-data" action="{{route('update-media')}}">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Media Title</label>
                                <input type="text" class="form-control" id="mediatitle" name="title" style="color:white;" placeholder="Enter Media" value="{{$media->title}}">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Category</label>
                                <Select class="form-control" name="category" style="color:white;">
                                    <option value=0>Select Category</option>

                                    @if($category)
                                    @foreach($category as $k=>$v)
                                    <option value="{{$v->id}}" {{($v->id==$media->category)?"Selected":" "}}>{{$v->name}}</option>
                                    @endforeach
                                    @endif

                                </Select>

                            </div>
                            <div class="form-group ">
                                <label class="form-label" for="exampleCheck1">Browse Media</label>
                                <input type="file" class="form-control" id="media" name="media_file" style="color:white;" >
                                <input type="hidden" class="form-control" name="media_file_name" value="{{$media->media_file}}">
                            </div>
                            <?php if ($media->categoryname->name == "Video") { ?>
                            <video width="320" height="240" controls>
                                <source src="{{asset('/uploads/' . $media->categoryname->name . '/'.$media->media_file)}}" type="video/mp4">
                            </video>
                            <?php } else if ($item->categoryname->name == "Images") { ?>
                            <img src="{{asset('/uploads/' . $item->categoryname->name . '/'.$item->media_file)}}" alt="Image" class="img-fluid">
                            <?php } else { ?>
                            <a href="{{asset('/uploads/' . $item->categoryname->name . '/'.$item->media_file)}}" alt="Image" class="img-fluid"><i class="fa fa-file-text-o">{{$item->media_file}}</i></a>
                            <?php } ?>
                            <div class="form-group">
                                <label class="form-label" for="exampleCheck1">Description</label>
                                <textarea class="form-control" id="media" name="description" style="color:white;">{{$media->description}}</textarea>
                            </div>
                            <input type="hidden" name="id" value="{{$media->id}}">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection