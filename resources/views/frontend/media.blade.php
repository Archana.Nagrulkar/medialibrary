<!DOCTYPE html>
<html lang="en">

<head>
  <title>Media Library &mdash;</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="{{URL::asset("frontend/css/bootstrap.min.css")}}">
  <link rel="stylesheet" href="{{URL::asset("frontend/css/magnific-popup.css")}}">
  <link rel="stylesheet" href="{{URL::asset("frontend/css/jquery-ui.css")}}">
  <link rel="stylesheet" href="{{URL::asset("frontend/css/owl.carousel.min.css")}}">
  <link rel="stylesheet" href="{{URL::asset("frontend/css/owl.theme.default.min.css")}}">

  <link rel="stylesheet" href="{{URL::asset("frontend/css/bootstrap-datepicker.css")}}">

  <link rel="stylesheet" href="{{URL::asset("frontend/fonts/flaticon/font/flaticon.css")}}">

  <link rel="stylesheet" href="{{URL::asset("frontend/css/aos.css")}}">
  <link rel="stylesheet" href="{{URL::asset("frontend/css/fancybox.min.css")}}">

  <link rel="stylesheet" href="{{URL::asset("frontend/css/style.css")}}">
  
</head>

<body>


  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="header-bar d-flex d-lg-block align-items-center" data-aos="fade-left">
      <div class="site-logo">
        <a href="index.html">Media Library</a>
      </div>

      <div class="d-inline-block d-xl-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

      <div class="main-menu">
        <ul class="js-clone-nav">
          <li class="active"><a href="{{route('main')}}">Home</a></li>

          @guest
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @if (Route::has('register'))
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
          </li>
          @endif
          @else
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
          @endguest
          <li><a href="{{route('add-media')}}">Upload Media</a></li>
          <li><a href="{{route('media')}}">View Media</a></li>
        </ul>
        <ul class="social js-clone-nav">
          <li><a href="#"><span class="icon-facebook"></span></a></li>
          <li><a href="#"><span class="icon-twitter"></span></a></li>
          <li><a href="#"><span class="icon-instagram"></span></a></li>
        </ul>
      </div>
    </header>
    <main class="main-content">
      <div class="container">
        <div class="container-fluid photos">

          @if($category)
          @foreach($category as $k=>$v)
          <div class="row">
            <h1>{{$v->name}}</h1>
            <div class="row align-items-stretch">
              @if($list)
              @foreach ($list as $item)
              @if($item->category==$v->id)
              <div class="col-6 col-md-6 col-lg-8" data-aos="fade-up">
                <a href="javascript:;" class="d-block photo-item">
                  <?php if ($item->categoryname->name == "Video") { ?>
                  <video width="320" height="240" controls>
                    <source src="{{asset('/uploads/' . $item->categoryname->name . '/'.$item->media_file)}}" type="video/mp4">
                  </video>
                  <?php } else if ($item->categoryname->name == "Images") { ?>
                  <img src="{{asset('/uploads/' . $item->categoryname->name . '/'.$item->media_file)}}" alt="Image" class="img-fluid">
                  <?php } else { ?>
                  <a href="{{asset('/uploads/' . $item->categoryname->name . '/'.$item->media_file)}}" alt="Image" class="img-fluid"><i class="fa fa-file-text-o">{{$item->media_file}}</i></a>
                  <?php } ?>
                  <div class="photo-text-more">
                    <div class="photo-text-more">
                      <h4>{{$item->title}}</h4>
                      <a href="{{URL::route('edit-media')}}/{{$item->id}}">Edit {{$item->categoryname->name}}</a><br>
                      <a href="{{URL::route('delete-media')}}/{{$item->id}}">Delete {{$item->categoryname->name}}</a><br>
                      @if(Auth::user()->role=='admin')
                       Uploaded By : {{$item->CreatedBy->name}}</a>

                @endif
              </div>
            </div>

            </a>
          </div>
          @endif
          @endforeach
          @endif
        </div>
      </div>
      @endforeach
      @endif
  </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-12 text-center py-5">
      <p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>
          document.write(new Date().getFullYear());
        </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
    </div>
  </div>
  </div>
  </main>

  </div> <!-- .site-wrap -->

  <script src="{{URL::asset("frontend/js/jquery-3.3.1.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/jquery-migrate-3.0.1.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/jquery-ui.js")}}"></script>
  <script src="{{URL::asset("frontend/js/popper.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/bootstrap.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/owl.carousel.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/jquery.stellar.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/jquery.countdown.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/jquery.magnific-popup.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/bootstrap-datepicker.min.js")}}"></script>
  <script src="{{URL::asset("frontend/js/aos.js")}}"></script>

  <script src="{{URL::asset("frontend/js/jquery.fancybox.min.js")}}"></script>

  <script src="{{URL::asset("frontend/js/main.js")}}"></script>

</body>

</html>